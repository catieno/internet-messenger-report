![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Static HTML site for Internet Messengers report.

Site will be redeployed automatically on every push and can be viewed here:
[Internet Messengers](http://dial.gitlab.io/internet-messenger-report/)

Copied with minimal changes from:
[Liquid Guava's Static Site](http://liquidguava.com/echodial)

---
